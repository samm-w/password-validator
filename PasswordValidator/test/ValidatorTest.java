/*
 * Name:Sam Whelan
 * Student Number: 991541458
 * Date: February 3rd 2021
 * */

import static org.junit.Assert.*;

import org.junit.Test;

public class ValidatorTest {
	
	@Test
	public void isValidLengthRegular() {
		boolean pswd = Validator.isValidLength("1234567890");
		assertTrue("Invalid length of password", pswd);
	}
	
	@Test
	public void isValidLengthException() {
		boolean pswd = Validator.isValidLength(null);
		assertFalse("Invalid length of password", pswd);
	}
	
	@Test
	public void isValidLengthBoundaryIn() {
		boolean pswd = Validator.isValidLength("12345678");
		assertTrue("Invalid length of password", pswd);
	}

	@Test
	public void isValidLengthBoundaryOut() {
		boolean pswd = Validator.isValidLength("1234567");
		assertFalse("Invalid length of password", pswd);
	}
	
	@Test
	public void hasTwoDigitsRegular() {
		boolean pswd = Validator.hasTwoDigits("1234567890");
		assertTrue("password doesn't contain at least 2 digits", pswd);
		
	}
	
	@Test
	public void hasTwoDigitsExceptionNoDigits() {
		boolean pswd = Validator.hasTwoDigits("Sam");
		assertFalse("password doesn't contain at least 2 digits", pswd);
		
	}
	
	@Test
	public void hasTwoDigitsExceptionNull() {
		boolean pswd = Validator.hasTwoDigits(null);
		assertFalse("password doesn't contain at least 2 digits", pswd);
		
	}
	
	@Test
	public void hasTwoDigitsBoundaryOut() {
		boolean pswd = Validator.hasTwoDigits("sam1");
		assertFalse("password doesn't contain at least 2 digits", pswd);
		
	}
	
	@Test
	public void hasTwoDigitsBoundaryIn() {
		boolean pswd = Validator.hasTwoDigits("sam12");
		assertTrue("password doesn't contain at least 2 digits", pswd);
		
	}
		
}
